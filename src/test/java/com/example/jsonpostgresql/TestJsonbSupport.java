package com.example.jsonpostgresql;

import com.example.jsonpostgresql.domain.JsonPerson;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

@Slf4j
public class TestJsonbSupport {

    @Test
    public void testJsonMapping() throws JsonProcessingException {
        log.info("... testJsonMapping ...");

        JsonPerson j = new JsonPerson();
        j.setName("John Wick");
        j.setAge(30);

        ObjectMapper mapper = new ObjectMapper();
        log.info(mapper.writeValueAsString(j));
    }

}
