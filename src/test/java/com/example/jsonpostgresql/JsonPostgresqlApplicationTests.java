package com.example.jsonpostgresql;

import com.example.jsonpostgresql.domain.JsonPerson;
import com.example.jsonpostgresql.domain.Person;
import com.example.jsonpostgresql.domain.PersonRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class JsonPostgresqlApplicationTests {

	@Autowired
	PersonRepository personRepository;

	@Test
	public void contextLoads() {
		JsonPerson jsonPerson = new JsonPerson();
		jsonPerson.setName("John Wick");
		jsonPerson.setAge(30);

		Person person = new Person();
		person.setJsonPerson(jsonPerson);
		Person persistedPerson = personRepository.save(person);

        Optional<Person> persisted = personRepository.findById(persistedPerson.getId());
    }

}
