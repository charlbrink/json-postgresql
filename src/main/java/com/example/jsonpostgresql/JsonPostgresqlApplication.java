package com.example.jsonpostgresql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JsonPostgresqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(JsonPostgresqlApplication.class, args);
	}
}
