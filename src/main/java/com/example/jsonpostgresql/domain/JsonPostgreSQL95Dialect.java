package com.example.jsonpostgresql.domain;

import org.hibernate.dialect.PostgreSQL95Dialect;

import java.sql.Types;

public class JsonPostgreSQL95Dialect extends PostgreSQL95Dialect {

    public JsonPostgreSQL95Dialect() {
        this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
    }
}