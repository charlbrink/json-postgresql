package com.example.jsonpostgresql.domain;

import lombok.Data;

import java.io.Serializable;

@Data
public class JsonPerson implements Serializable {
    private String name;
    private int age;
}
